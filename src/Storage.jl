# %%
# dev ../../repo/agei2/IOPipe/Storage
module Storage
using JSON2
using JLD2
using FileIO
using FastClosures
using InteractiveUtils


struct FileTypeConfig
	dataname::String
	version::String
	extension::String
	ext::Symbol
end
Base.getproperty(file::FileTypeConfig, sym::Symbol) = 
if sym == :file_name "$(file.dataname).$(file.version).$(file.extension)" 
else
	getfield(file, sym)
end

function parse_extension(ext)
  return ext == "json" ? :JSON : (ext == "cloud" ? :CLOUD : (ext == "firebase" ? :FIREBASE : (ext == "jld2" ? :JLD2 : :UNKNOWN)))
end

function filename_splitter(filename)
	file_parts::Vector{String} = split(filename, ".")
	dataname, version, extension = file_parts[1],"",file_parts[end]
	length(file_parts) == 3 && (version = file_parts[2])
	FileTypeConfig("$dataname", "$version", "$extension", parse_extension(extension))
end

function path_splitter(path)
	url_parts::Vector{String} = split(path, "/")
	length(url_parts) == 3 && return url_parts[1], url_parts[2], url_parts[end] 
	length(url_parts) == 4 && return url_parts[1], join(url_parts[2:end-1],"/"), url_parts[end] 
	length(url_parts) == 5 && return join(url_parts[1:2],"/"), join(url_parts[3:end-1],"/"), url_parts[end] 
	@assert false  "{$path} should follow format: x_data/[run_id]/data_name.version.extension"
end

read_all(url) = open(f->read(f, String),url)
write_all(data)= (
		println(typeof(data));
		extension == :JLD2 && return FileIO.save(pointer, Dict("dataset" => data));
		# typeof(data) is byte
		open(pointer, typeof(data) ? "wb" : "w")  do file
			file.write(data)
		end
)
function loader(abs_path, storage_type)
	baseurl, url, data_file = path_splitter(abs_path)
	file = filename_splitter(data_file)

	load_fn = let file=file; path="$baseurl/$url/$data_file";
		(d_type; verbose=false) -> (
			verbose && println("Loading: $d_type -> $(file.file_name)");
			if file.ext == :JSON
				return JSON2.read(read_all(path))
			elseif file.ext == :JLD2
				return read_all(path)
			end
		)
	end

	save_fn = let file = file;
			(d_type, data) -> (
				println("Saving: $d_type -> $(file.file_name)"); 
				if file.ext == :JSON
					return write_all(JSON.write(data))
				elseif file.ext == :JLD2
					return write_all(data)
				end
			)
	end
	return save_fn, load_fn, file
end
# @code_warntype storage_obj("_1_datasets/crypto_data/crypto.0.jld2", :File)
# @code_warntype loader("_1_datasets/crypto_data/crypto.0.jld2", :File)
# loader("_1_datasets/crypto_data/crypto.0.jld2", :File)

end  # module Storage
